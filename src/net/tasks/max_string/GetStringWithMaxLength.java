package net.tasks.max_string;

public class GetStringWithMaxLength {

    public static void main(String[] args) {
        String [] names = {"Greg", "Aleksandra", "Martha", "Oliwka"};
        String longestName = findLongestString(names);
        System.out.println( longestName);

    }

    private static String findLongestString(String [] names){
        int size = names.length;
        String longestName = names[0];
        for(int i = 0; i < size; i++){
            if(names[i].length() > longestName.length()){
                longestName = names[i];
            }}
        return longestName;
    }
}
