package net.tasks.power_of_two;

public class PowerOfTwo {

    public static void main(String[] args) {
        System.out.println(checkPowerOfTwo(-2));
        System.out.println(checkPowerOfTwo(1));
        System.out.println(checkPowerOfTwo(4));
        System.out.println(checkPowerOfTwo(8));
        System.out.println(checkPowerOfTwo(5));
        System.out.println(checkPowerOfTwo(0));

    }


    private static String checkPowerOfTwo(int number) {
        String result = null;
        if ((number <= 0) || (number == 1)){
            result = String.format("%d this is not a power of two", number);
        } else if (number % 2 != 0) {
            result = String.format("%d this is not a power of two", number);
        } else {
            for (int i = 0; i <= number; i++) {
                if (number == Math.pow(2, i))
                    result = String.format("%d this is a power of two", number);
            }
        }
        return result;
    }
}
