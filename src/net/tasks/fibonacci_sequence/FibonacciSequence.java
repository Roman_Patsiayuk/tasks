package net.tasks.fibonacci_sequence;

import java.util.ArrayList;
import java.util.List;

public class FibonacciSequence {

    public static void main(String[] args) {
        System.out.println(fibonacciSequence(7));
        System.out.println(sumOfFibonacciSequence(fibonacciSequence(7)));
        System.out.println(sumOfEvenElementsFromFibonacciSequence(fibonacciSequence(7)));
        System.out.println(sumOfOddElementsFromFibonacciSequence(fibonacciSequence(7)));
    }

    private static int recursionFibonacci(int number) {
        if (number < 0) {
            throw new IllegalArgumentException("Negative number");
        } else if (number == 0)
            return 0;
        else if (number == 1 || number == 2)
            return 1;
        else
            return recursionFibonacci(number - 1) + recursionFibonacci(number - 2);

    }

    private static List<Integer> fibonacciSequence(int number) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i <= number; i++) {
            list.add(recursionFibonacci(i));
        }
        return list;
    }

    public int fibonacci(int number) {
        int a = 1;
        int b = 1;
        int fibonacciResult = 1;
        if (number < 0) {
            throw new IllegalArgumentException("Negative number");
        } else if (number == 0) {
            return 0;
        } else if (number == 1 || number == 2) {
            return 1;
        } else {
            for (int i = 3; i < number; i++) {
                fibonacciResult = a + b;
                a = b;
                b = fibonacciResult;
            }
        }
        return fibonacciResult;
    }

    //https://www.concretepage.com/java/jdk-8/java-8-sum-array-map-and-list-collection-example-using-reduce-and-collect-method
    private static int sumOfFibonacciSequence(List<Integer> list) {
        //return list.stream().mapToInt(Integer::intValue).sum();
        //return list.stream().collect(Collectors.summingInt(Integer::intValue));
        //return list.stream().reduce(0, (x,y) -> x+y);
        return list.stream().reduce(0, Integer::sum);
    }

    private static int sumOfEvenElementsFromFibonacciSequence(List<Integer> list) {
        //return list.stream().filter(i -> i % 2 == 1).mapToInt(Integer::intValue).sum();
        //return list.stream().filter(i -> i % 2 == 1).collect(Collectors.summingInt(Integer::intValue));
        //return list.stream().filter(i -> i % 2 == 1).reduce(0, (x,y) -> x+y);
        return list.stream().filter(i -> i % 2 == 1).reduce(0, Integer::sum);
    }

    private static int sumOfOddElementsFromFibonacciSequence(List<Integer> list) {
        //return list.stream().filter(i -> i % 2 == 0).mapToInt(Integer::intValue).sum();
        //return list.stream().filter(i -> i % 2 == 0).collect(Collectors.summingInt(Integer::intValue));
        //return list.stream().filter(i -> i % 2 == 0).reduce(0, (x,y) -> x+y);
        return list.stream().filter(i -> i % 2 == 0).reduce(0, Integer::sum);
    }
}
