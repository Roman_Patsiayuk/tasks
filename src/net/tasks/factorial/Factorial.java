package net.tasks.factorial;

public class Factorial {

    public static void main(String[] args) {
        //System.out.println(factorial(1));
        System.out.println(factorial(5));
        //System.out.println(factorial(0));
    }

    //https://docs.oracle.com/javase/8/docs/api/java/util/stream/IntStream.html
    private static int factorial(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException();
        } else if (number == 1) {
            return 1;
        } else {
            //return IntStream.rangeClosed(2, number).reduce(1, (x, y) -> x * y);
            return number * factorial(number - 1);
        }
    }
}
